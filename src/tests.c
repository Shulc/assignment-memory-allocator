#include "../include/tests.h"
#include "../include/mem.h"
void* _map_pages(void const* addr, size_t length, int additional_flags);
void debug(const char *fmt, ...);
void debug_block(struct block_header* b, const char* fmt, ... );
void* _block_after( struct block_header const* block );
bool _blocks_continuous (struct block_header const* fst, struct block_header const* snd );
void* heap;


void test_heap_init(){

    debug("\n------------------------------------------\nTest of initializing heap\n");
    heap = heap_init(100);
    if (heap == NULL){
        printf("error occurred while initializing a heap\n");
        return;
    }
    debug_heap(stdout,heap);
    printf("\ninitialization was successful!\n");
}

void test_allocate_memory(){
    debug("\n------------------------------------------\nTest of allocating memory\n" );
    void* temp = _malloc(228);
    if(temp==NULL){
        printf("Error occurred. Memory wasn't allocated\n");
        return;
    }
    struct block_header* header = block_get_header(temp);
    debug_block(header, "allocated block: ");
    debug("\nMemory allocating was successful!\n");
    _free(temp);
    if(header->is_free==false){
        printf("Error occurred. Memory wasn't freed\n");
        return;
    }
    debug_block(header, "block after freeing memory: ");
    debug("\nFreeing was successful!\n");
}
void test_several_blocks(){
    debug("\n------------------------------------------\nTest of freeing one and two blocks of several\n");
    void* block_0 = _malloc(228);
    void* block_1 = _malloc(337);
    void* block_2 = _malloc(119);
    if (block_0==NULL ||  block_1==NULL ||  block_2==NULL){
        debug("Error occurred. Memory wasn't allocated for several blocks\n");
        return;
    }
    struct block_header* header_0 = block_get_header(block_0);
    struct block_header* header_1 = block_get_header(block_1);
    struct block_header* header_2 = block_get_header(block_2);
    debug_block(header_0, "allocated block_0: ");
    debug_block(header_1, "allocated block_1: ");
    debug_block(header_2, "allocated block_2: ");
    debug("\n");
    _free(block_1);
    if(header_1->is_free==false) {
        debug("\nError occurred. Memory of first block wasn't freed\n");
        return;
    }
    if ((header_0->is_free==true) || (header_2->is_free==true)) {
        debug("\nError occurred. Memory of other blocks was freed\n");
        return;
    }
    debug_block(header_0, "allocated block_0: ");
    debug_block(header_1, "freed block_1: ");
    debug_block(header_2, "allocated block_2: ");
    debug("\nFreeing one block of several was successful\n\n");

    _free(block_0);
    if (header_0->is_free==false){
        debug("\nError occurred. Memory of second block wasn't freed\n");
        return;
    }
    if (header_2->is_free==true){
        debug("\nError occurred. Memory of another block was freed\n");
        return;
    }
    debug_block(header_0, "freed block_0: ");
    debug_block(header_1, "freed block_1: ");
    debug_block(header_2, "allocated block_2: ");
    debug("\nFreeing two blocks of several was successful\n");
    _free(block_2);
}

void test_grow_heap(){
    debug("\n------------------------------------------\nTest of growing heap\nHeap before allocating memory for big block\n");
    debug_heap(stdout, heap);
    void* temp = _malloc(10500);
    if(temp==NULL){
        debug("error occurred while allocating memory for big block\n");
        return;
    }
    debug("Heap after allocating memory for big block");
    debug_heap(stdout, heap);
    _free(temp);
    debug("Heap after freeing memory for big block");
    debug_heap(stdout, heap);
    debug("\nGrowing heap was successful\n");
}

void test_lack_of_memory(){
    debug("\n------------------------------------------\nTest of allocating memory in new region\n");
    void* temp_0 = _malloc(100500);
    struct block_header* last = block_get_header(temp_0);
    while(last->next != NULL) {
        last = last->next;
    }
    debug_heap(stdout,heap);
    void* next = _block_after(last);
    struct block_header* next_header = block_get_header(next);
    _map_pages( next, 100500, PROT_NONE);
    void* temp_1 = _malloc(100500);
    debug_block(last,"last block of heap");
    debug_block(next_header, "  block after heap");
    debug_block(block_get_header(temp_1), " new block in heap");
    if (temp_1==next){
        debug("\nError occurred. new block allocated incorrectly\n");
        return;
    }
    debug_heap(stdout,heap);
    _free(temp_0);
    _free(temp_1);
    debug("\nMemory was allocated successful\n");
}
void merging_blocks_not_a_test(){
    void* a = _malloc(100);
    debug("\n------------------------------------------\nMerging several blocks\n");
    debug("Before merging\n");
    debug_heap(stdout, heap);
    _free(a);
    debug("\nAfter merging\n");
    debug_heap(stdout, heap);
}
void run_all_tests(){
    test_heap_init();
    test_allocate_memory();
    test_several_blocks();
    test_grow_heap();
    test_lack_of_memory();
    merging_blocks_not_a_test();
}