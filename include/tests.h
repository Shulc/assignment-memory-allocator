#ifndef ForTests
#define ForTests
#include "mem.h"
#include "mem_internals.h"
void test_heap_init();
void test_allocate_memory();
void test_several_blocks();
void test_grow_heap();
void test_lack_of_memory();
void merging_blocks_not_a_test();
void run_all_tests();
#endif